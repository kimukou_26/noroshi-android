package jp.co.noroshi;

import java.io.File;
import java.util.EventListener;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.OAuthAuthorization;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.media.MediaProvider;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.SharedPreferences;

//Twitterへの認証処理
public class TwitterAction{

	private static final String TAG = "noroshi_TwitterAction";
	private static final String OAUTH_VERIFIER = "oauth_verifier";
	private Activity _activity;
	private Handler _handler;
	private Twitter _twitter;
	private OAuthResultListener _oAuthResultListener = null;
	private OAuthAuthorization _oAuthAuthorization;
	private int _errorStatusCode = 0;
	private String _tweet;
	private UploadListener _uploadListener = null;

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public TwitterAction(Activity act){
		_activity = act;
		_handler = new Handler();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private Configuration settingConsumerKey(){
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true);
		cb.setOAuthConsumerKey(Config.TWITTER_CONSUMERKEY);
		cb.setOAuthConsumerSecret(Config.TWITTER_CONSUMERSECRET);
		return cb.build();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private Configuration settingAccessToken(String AccessToken,String AccessTokenSecret){
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true);
		cb.setOAuthConsumerKey(Config.TWITTER_CONSUMERKEY);
		cb.setOAuthConsumerSecret(Config.TWITTER_CONSUMERSECRET);
		cb.setOAuthAccessToken(AccessToken);
		cb.setOAuthAccessTokenSecret(AccessTokenSecret);
		cb.setMediaProvider(MediaProvider.TWITTER.toString());
		return cb.build();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	//AccessTokenの設定
	public void setAccessToken(String AccessToken,String AccessTokenSecret){
		_twitter = new TwitterFactory(settingAccessToken(AccessToken, AccessTokenSecret)).getInstance();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	//認証ページのURLを取ってくる
	public void startOAuth(){
		_oAuthAuthorization = new OAuthAuthorization(settingConsumerKey());
		// これが無いとgetOAuthRequestToken()で例外が発生するらしい
		_oAuthAuthorization.setOAuthAccessToken(null);

		//マルチスレッドにしないとAndroid3.0以降の端末では例外処理が発生する
		new Thread(new Runnable() {

			private String url = null;

			@Override
			public void run() {
				//アプリの認証オブジェクト作成
				RequestToken req = null;
				try {
					req = _oAuthAuthorization.getOAuthRequestToken();
				} catch (TwitterException e) {
					e.printStackTrace();
					oAuthErrorHandler(e.getStatusCode());
				}
				if(req != null){
					url = req.getAuthorizationURL();
				}else{
					//エラー原因が不明
					oAuthErrorHandler(Config.UNKOWN_ERROR_STATUS_CODE);
				}
				//メインスレッドに処理を投げる
				_handler.post(new Runnable() {
					@Override
					public void run() {
						if(_oAuthResultListener != null){
							_oAuthResultListener.requestOAuthUrl(url);
						}
					}
				});
			}
		}).start();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	//認証完了後AccessTokenを取得する処理
	//(CallbackURLにCallBackされればAccessTokeが取得することが可能になる)
	public void returnOAuth(final String url){

		//マルチスレッドにしないとAndroid3.0以降の端末では例外処理が発生する
		new Thread(new Runnable() {

			private AccessToken accessToken = null;

			@Override
			public void run() {
				try {
					//accessTokenを取得するためのパラメータ(oauth_verifier)がCallBackURLの中にあるのでそれの値を取ってきて認証を行う
					Uri uri = Uri.parse(url);
					String oauth_verifier = uri.getQueryParameter(OAUTH_VERIFIER);
					//AccessTokenを取得する
					accessToken = _oAuthAuthorization.getOAuthAccessToken(oauth_verifier);
					//AccessTokenを記録する
					recordAccessToken(accessToken.getToken(),accessToken.getTokenSecret());
					//メインスレッドに処理を投げる
					_handler.post(new Runnable() {
						@Override
						public void run() {
							if(_oAuthResultListener != null){
								_oAuthResultListener.oAuthResult(accessToken.getToken(), accessToken.getTokenSecret());
							}
						}
					});
				} catch (TwitterException e) {
					e.printStackTrace();
					oAuthErrorHandler(e.getStatusCode());
				}
			}
		}).start();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	//取得したAccessTokenをローカルに保存する
	private void recordAccessToken(String AccessToken,String AccessTokenSecret){
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(_activity);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString(_activity.getString(R.string.TwitterAccessTokenKey), AccessToken);
		editor.putString(_activity.getString(R.string.TwitterAccessTokenSecretKey), AccessTokenSecret);
		editor.commit();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	//認証処理中にエラーが出たときに行う処理
	private void oAuthErrorHandler(int StatusCode){
		_errorStatusCode = StatusCode;
		//メインスレッドに処理を投げる
		_handler.post(new Runnable() {
			@Override
			public void run() {
				if(_oAuthResultListener != null){
					_oAuthResultListener.oAuthError(_errorStatusCode);
				}
			}
		});
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * リスナーを追加する
	 */
	public void setOnOAuthResultListener(OAuthResultListener OAuthListener){
		_oAuthResultListener = OAuthListener;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * リスナーを削除する
	 */
	public void removeOAuthResulListener(){
		_oAuthResultListener = null;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	//処理が終わったことを通知する独自のリスナーを作成
	public interface OAuthResultListener extends EventListener {

		//Twitterの認証ページのURLを取得した場合に呼ばれる
		public void requestOAuthUrl(String url);

		//Twitterの認証時にエラーが発生した場合に呼ばれる
		public void oAuthError(int StatusCode);

		//Twitterの認証処理が完了し、AccessTokenが取得できる時に呼ばれる
		public void oAuthResult(String token,String tokenSecret);

	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void sendTweetToTwitter(String tweet){
		_tweet = tweet;

		//マルチスレッドにしないとAndroid3.0以降の端末では例外処理が発生する
		new Thread(new Runnable() {

			private int StatusCode = 0;

			@Override
			public void run() {
				try {
					//ツイートする内容の設定
					StatusUpdate status = new StatusUpdate(_tweet);
					//twitterと通信し、ツイートを投稿する
					_twitter.updateStatus(status);

					//メインスレッドに処理を投げる
					_handler.post(new Runnable() {
						@Override
						public void run() {
							if(_uploadListener != null){
								_uploadListener.success(_tweet);
							}
						}
					});
				} catch (TwitterException e) {
					e.printStackTrace();
					StatusCode = e.getStatusCode();
					//メインスレッドに処理を投げる
					_handler.post(new Runnable() {
						@Override
						public void run() {
							if(_uploadListener != null){
								_uploadListener.error(StatusCode);
							}
						}
					});
				}
			}
		}).start();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * リスナーを追加する
	 */
	public void setOnUploadListener(UploadListener uploadListener){
		_uploadListener = uploadListener;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * リスナーを削除する
	 */
	public void removeUploadListener(){
		_uploadListener = null;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	//処理が終わったことを通知する独自のリスナーを作成
	public interface UploadListener extends EventListener {

		//Twitterへの画像の投稿が完了した場合に呼ばれる
		public void success(String Tweet);

		//Twitterへの画像の投稿に失敗した場合に呼ばれる
		public void error(int StatusCode);

	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------


}
