﻿package jp.co.noroshi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Window;
import android.widget.ImageView;

public class SprashActivity extends Activity {

	//次のActivity画表示されるまでの時間
	private static final int START_SCREEN_DISPLAY_TIME = 1000; // Millisecond
	private static String TAG = "noroshi_SprashActivity";

	// ---------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(ExtraLayout.getParenetView(this, R.layout.sprash_view));

		//画像を表示
		ImageView sprashImage = (ImageView) findViewById(R.id.SprashImage);
		sprashImage.setImageResource(R.drawable.noroshiandsp);
		Handler handler = new Handler(new Handler.Callback() {
			@Override
			public boolean handleMessage(Message msg) {
				//次のactivityを実行
				Intent intent = new Intent(SprashActivity.this, TopActivity.class);
				startActivity(intent);
				finish();
				return true;
			}
		});
		handler.sendEmptyMessageDelayed(0, this.START_SCREEN_DISPLAY_TIME);
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Tools.releaseImageView((ImageView) findViewById(R.id.SprashImage));
	}

	// ---------------------------------------------------------------------------------------------------------------------------------------------------------------
}
