package jp.co.noroshi;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;

//主に複数の画像を合成して一つの画像にすることで、メモリの使用量を抑える
public class CompositeImageCanvas extends Canvas{

	private static final String TAG = "noroshi_CompositeImageCanvas";
	private Bitmap _baseImage;

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public CompositeImageCanvas(Bitmap baseImage) {
		super(baseImage);
		_baseImage = baseImage;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void incorporateImage(Bitmap image){
		this.drawBitmap(image, new Rect(0,0,image.getWidth(),image.getHeight()), new Rect(0,0,image.getWidth(),image.getHeight()), new Paint());
		this.releaseImage(image);
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void incorporateImage(Bitmap image,Rect src , Rect dst, Paint paint){
		this.drawBitmap(image, src, dst, paint);
		this.releaseImage(image);
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void incorporateImage(Bitmap image,Matrix matrix, Paint paint){
		this.drawBitmap(image, matrix, paint);
		this.releaseImage(image);
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public Bitmap getConpositeImage(){
		return _baseImage;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void release(){
		if(_baseImage != null){
			_baseImage.recycle();
			_baseImage = null;
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private void releaseImage(Bitmap image){
		if(image != null){
			image.recycle();
			image = null;
		}
	}
	//---------------------------------------------------------------------------------------------------------------------------------------------------------------
}