package jp.co.noroshi;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;

public class TopActivity extends Activity {

	// ---------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(ExtraLayout.getParenetView(this, R.layout.top_view));

		ImageView backgroundImage = (ImageView) findViewById(R.id.BackgroundImage);
		backgroundImage.setImageResource(R.drawable.homeandbg);
		ImageView putUpImageView = (ImageView) findViewById(R.id.PutUpImageView);
		ExtraLayout.setBaseImageView(this, putUpImageView, R.drawable.homeand01off);
		putUpImageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent putUpIntent = new Intent(TopActivity.this, PutUpNoroshiActivity.class);
				startActivity(putUpIntent);
			}
		});
		putUpImageView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				ImageView image = (ImageView) v;
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					image.setImageResource(R.drawable.homeand01on);
					break;
				case MotionEvent.ACTION_CANCEL:
					image.setImageResource(R.drawable.homeand01off);
					break;
				case MotionEvent.ACTION_UP:
					image.setImageResource(R.drawable.homeand01off);
					break;
				case MotionEvent.ACTION_OUTSIDE:
					image.setImageResource(R.drawable.homeand01off);
					break;
				}
				return false;
			}
		});

		ImageView cameraImageView = (ImageView) findViewById(R.id.CameraImageView);
		ExtraLayout.setBaseImageView(this, cameraImageView, R.drawable.homeand02off);
		cameraImageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent cameraIntent = new Intent(TopActivity.this, CameraActivity.class);
				startActivity(cameraIntent);
			}
		});
		cameraImageView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				ImageView image = (ImageView) v;
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					image.setImageResource(R.drawable.homeand02on);
					break;
				case MotionEvent.ACTION_CANCEL:
					image.setImageResource(R.drawable.homeand02off);
					break;
				case MotionEvent.ACTION_UP:
					image.setImageResource(R.drawable.homeand02off);
					break;
				case MotionEvent.ACTION_OUTSIDE:
					image.setImageResource(R.drawable.homeand02off);
					break;
				}
				return false;
			}
		});
	}

	// ---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public static OnTouchListener _imageTouchListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				((ImageView) v).setColorFilter(new LightingColorFilter(Color.LTGRAY, 0));
				break;
			case MotionEvent.ACTION_CANCEL:
				((ImageView) v).clearColorFilter();
				break;
			case MotionEvent.ACTION_UP:
				((ImageView) v).clearColorFilter();
				break;
			case MotionEvent.ACTION_OUTSIDE:
				((ImageView) v).clearColorFilter();
				break;
			}
			return false;
		}
	};

	// ---------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Tools.releaseImageView((ImageView) findViewById(R.id.BackgroundImage));
	}

	// ---------------------------------------------------------------------------------------------------------------------------------------------------------------
}
