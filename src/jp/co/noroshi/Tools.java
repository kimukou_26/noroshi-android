package jp.co.noroshi;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;

public class Tools {

	private static final String TAG = "noroshi_Tools";

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public static int getCameraDisplayOrientation(Activity act,int nCameraID){
		if(Build.VERSION.SDK_INT >= 9){
			Camera.CameraInfo info = new Camera.CameraInfo();
			Camera.getCameraInfo(nCameraID, info);
			int rotation = act.getWindowManager().getDefaultDisplay().getRotation();
			int degrees = 0;
			switch (rotation) {
				//portate:縦向き
				case Surface.ROTATION_0: degrees = 0; break;
				//landscape:横向き
				case Surface.ROTATION_90: degrees = 90; break;
				case Surface.ROTATION_180: degrees = 180; break;
				case Surface.ROTATION_270: degrees = 270; break;
			}
			int result;
			//Camera.CameraInfo.CAMERA_FACING_FRONT:アウトカメラ
			//Camera.CameraInfo.CAMERA_FACING_BACK:インカメラ

			if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
				result = (info.orientation + degrees) % 360;
				result = (360 - result) % 360;  // compensate the mirror
			} else {  // back-facing
				result = (info.orientation - degrees + 360) % 360;
			}
			return result;
		}
		return 90;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public static void memoryInfo(Context con) {
		ActivityManager activityManager = (ActivityManager)con.getSystemService(Activity.ACTIVITY_SERVICE);
		ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
		activityManager.getMemoryInfo(memoryInfo);

		//使えるメモリ容量全体
		Log.d(TAG, "memoryInfo.availMem[MB] = " + (int)(memoryInfo.availMem/1024/1024));
		//backgroundで実行しているものを次々とkillしていくしきい値
		Log.d(TAG, "memoryInfo.threshold[MB] = " + (int)(memoryInfo.threshold/1024/1024));
		//
		Log.d(TAG, "memoryInfo.lowMemory = " + memoryInfo.lowMemory);

		/*
		// 確保しているヒープサイズ
		Log.d(TAG, "NativeHeapSize = " + android.os.Debug.getNativeHeapSize()/1024);
		// 空きヒープサイズ
		Log.d(TAG, "NativeHeapFreeSize = " + android.os.Debug.getNativeHeapFreeSize()/1024);
		// 使用中ピープサイズ
		Log.d(TAG, "NativeHeapAllocatedSize = " + android.os.Debug.getNativeHeapAllocatedSize()/1024);
		*/

		/*
		// 確保しているヒープサイズ
		Log.d(TAG, mes + ":NativeHeapSize = " + android.os.Debug.getNativeHeapSize());
		// 空きヒープサイズ
		Log.d(TAG, mes + ":NativeHeapFreeSize = " + android.os.Debug.getNativeHeapFreeSize());
		// 使用中ピープサイズ
		Log.d(TAG, mes + ":NativeHeapAllocatedSize = " + android.os.Debug.getNativeHeapAllocatedSize());
		*/

		/*
		// アプリのメモリ情報を取得
		Runtime runtime = Runtime.getRuntime();
		// トータルメモリ
		Log.v(TAG, "totalMemory[KB] = " + runtime.totalMemory()/1024);
		// 空きメモリ
		Log.v(TAG, "freeMemory[KB] = " + runtime.freeMemory()/1024);
		//現在使用しているメモリ
		Log.v(TAG, "usedMemory[KB] = " + (runtime.totalMemory() - runtime.freeMemory())/1024);
		// Dalvikで使用できる最大メモリ
		Log.v(TAG, "maxMemory[KB] = " + runtime.maxMemory()/1024);
		*/
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public static void releaseImageView(ImageView imageView){
		if (imageView != null) {
			BitmapDrawable bitmapDrawable = (BitmapDrawable)(imageView.getDrawable());
			if (bitmapDrawable != null) {
				bitmapDrawable.setCallback(null);
			}
			imageView.setImageBitmap(null);
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	//WebViewを使用したときのメモリリーク対策
	public static void releaseWebView(WebView webview){
		webview.stopLoading();
		webview.setWebChromeClient(null);
		webview.setWebViewClient(null);
		webview.destroy();
		webview = null;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public static String makeUrlParams(Bundle params){
		Set<String> keys = params.keySet();
		ArrayList<String> paramList = new ArrayList<String>();
		for (String key : keys) {
			paramList.add(key + "=" + params.get(key).toString());
		}
		return Tools.join(paramList, "&");
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public static String makeUrlParams(Map<String, Object> params){
		Set<String> keys = params.keySet();
		ArrayList<String> paramList = new ArrayList<String>();
		for(Entry<String, Object> e : params.entrySet()) {
			paramList.add(e.getKey() + "=" + e.getValue().toString());
		}
		return Tools.join(paramList, "&");
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public static String join(String[] list, String with) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < list.length; i++) {
		if (i != 0) { buf.append(with);}
			buf.append(list[i]);
		}
		return buf.toString();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public static String join(ArrayList<String> list, String with) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < list.size(); i++) {
			if (i != 0) { buf.append(with);}
			buf.append(list.get(i));
		}
		return buf.toString();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public static final void putObjectToContentValues(ContentValues cv, String key, Object value){
		if(value instanceof String){
			String v = (String) value;
			cv.put(key, v);
		}else if(value instanceof Byte){
			byte v = (Byte) value;
			cv.put(key, v);
		}else if(value instanceof byte[]){
			byte[] v = (byte[]) value;
			cv.put(key, v);
		}else if(value instanceof Integer){
			int v = (Integer) value;
			cv.put(key, v);
		}else if(value instanceof Float){
			float v = (Float) value;
			cv.put(key, v);
		}else if(value instanceof Short){
			short v = (Short) value;
			cv.put(key, v);
		}else if(value instanceof Double){
			double v = (Double) value;
			cv.put(key, v);
		}else if(value instanceof Long){
			long v = (Long) value;
			cv.put(key, v);
		}else if(value instanceof Boolean){
			boolean v = (Boolean) value;
			cv.put(key, v);
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public static final void putCursorToBundle(Bundle b, Cursor c){
		String[] columnNames = c.getColumnNames();
		for(int i = 0;i < columnNames.length; ++i){
			String key = columnNames[i];
			String value = c.getString(i);
			b.putString(key, value);
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	//ネットワークに接続されているかどうかの判別
	public static boolean checkNetWork(Context context){
		ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		if(networkInfo != null){
			return connectivityManager.getActiveNetworkInfo().isConnected();
		}
		return false;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	//Toastの表示
	public static void showToast(Context con, String message) {
		Toast toast = Toast.makeText(con, message, Toast.LENGTH_LONG);
		toast.show();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------
}
