package jp.co.noroshi;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.view.Menu;

public class DBOpenHelper extends SQLiteOpenHelper {

	private static final String TAG = "noroshi_DBOpenHelper";

	//context, DB名, カーソル, バージョン番号
	//データベースのクエリ結果に対してカーソルが特別な操作や検証などをやるような
	//第三引数は自作のカーソルクラスを拡張したものを作るときには、このカーソルを渡すもの。デフォルトの設定でいい時はnull
	public DBOpenHelper(Context context, String DBName, int version) {
		super(context, DBName, null, version);
	}

	public DBOpenHelper(Context context, String DBName, CursorFactory factory,int version) {
		super(context, DBName, factory, version);
	}

	//初回はcreateされるが二回目以降はcreateされない
	@Override
	public void onCreate(SQLiteDatabase db) {
		ArrayList<String> SQLList = MigrationSettings.createTable();
		SQLList.addAll(MigrationSettings.createIndex());
		for(int i = 0; i < SQLList.size(); ++i){
			db.execSQL(SQLList.get(i));
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

}
