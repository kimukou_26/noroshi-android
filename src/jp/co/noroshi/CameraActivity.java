package jp.co.noroshi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import jp.co.noroshi.AsynkWebAPIRequest.RequestFinishCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageButton;

public class CameraActivity extends Activity {

	private static final String TAG = "noroshi_CameraActivity";
	private static final int TIME_LIMIT_MINUTE = 30;
	private int _cameraId = 0;
	private CameraOverlayView _cameraOverlayView;
	//地磁気
	private float[] _geomagnetic   = new float[3];
	//重力
	private float[] _accelerometer       = new float[3];
	//向き
	private float[] _orientation   = new float[3];

	private SensorManager _sensorManager;
	private LocationManager _locationManager;

	private Camera _camera = null;
	private Map<String, Bitmap> _noroshiImages;

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private SensorEventListener _sensorEventListener = new SensorEventListener(){
		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {

		}

		@Override
		public void onSensorChanged(SensorEvent event) {
			switch(event.sensor.getType()) {
			case Sensor.TYPE_ACCELEROMETER:
				_accelerometer = event.values.clone();
				break;
			case Sensor.TYPE_MAGNETIC_FIELD:
				_geomagnetic = event.values.clone();
				break;
			}
			if(_geomagnetic != null && _accelerometer != null){
				/* 回転行列 */
				float[]  inR = new float[16];
				float[] outR = new float[16];
				SensorManager.getRotationMatrix(inR, null, _accelerometer, _geomagnetic);
				//Activityの表示が縦固定の場合。横向きになる場合、修正が必要です
				SensorManager.remapCoordinateSystem(inR, SensorManager.AXIS_X, SensorManager.AXIS_Z, outR);
				SensorManager.getOrientation(outR, _orientation);
				//_orientation[0]:Z軸 _orientation[1]:X軸 _orientation[2]:Y軸
				//Log.d(TAG, "pitch(X軸):"+Math.toDegrees(_orientation[1])+" roll(Y軸):"+Math.toDegrees(_orientation[2])+" azimuth(Z軸):"+Math.toDegrees(_orientation[0]));
				_cameraOverlayView.setDeviceOrientaion(Math.toDegrees(_orientation[1]), Math.toDegrees(_orientation[2]), Math.toDegrees(_orientation[0]));
				_accelerometer = null;
				_geomagnetic = null;
			}
		}
	};

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private LocationListener _locationListener = new LocationListener(){

		@Override
		public void onLocationChanged(Location location) {
			requestGoogleAPI(location);
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(CameraActivity.this);
			SharedPreferences.Editor editor = sp.edit();
			editor.putString(CameraActivity.this.getString(R.string.usingProviderKey), location.getProvider());
			editor.commit();
		}

		@Override
		public void onProviderDisabled(String provider) {

		}

		@Override
		public void onProviderEnabled(String provider) {

		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {

		}

	};

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.noroshi_camera_view);
		_cameraId = getIntent().getIntExtra(getResources().getString(R.string.IntentCameraIdKey), 0);

		_cameraOverlayView = (CameraOverlayView) findViewById(R.id.CameraOverrideView);
		_sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
		_locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		_noroshiImages = new HashMap<String, Bitmap>();
		Bitmap noroshi50mb = BitmapDecodeHelper.getBitmap(this, R.drawable.noroshi50mb);
		_noroshiImages.put("noroshi50mb", ExtraLayout.resizeBaseBitmap(this, noroshi50mb));
		Bitmap noroshi50mp = BitmapDecodeHelper.getBitmap(this, R.drawable.noroshi50mp);
		_noroshiImages.put("noroshi50mp", ExtraLayout.resizeBaseBitmap(this, noroshi50mp));
		Bitmap noroshi50mw = BitmapDecodeHelper.getBitmap(this, R.drawable.noroshi50mw);
		_noroshiImages.put("noroshi50mw", ExtraLayout.resizeBaseBitmap(this, noroshi50mw));
		Bitmap noroshi50my = BitmapDecodeHelper.getBitmap(this, R.drawable.noroshi50my);
		_noroshiImages.put("noroshi50my", ExtraLayout.resizeBaseBitmap(this, noroshi50my));
		Bitmap smoke02 = BitmapDecodeHelper.getBitmap(this, R.drawable.smoke02);
		_noroshiImages.put("smoke02", ExtraLayout.resizeBaseBitmap(this, smoke02));
		Bitmap smoke03 = BitmapDecodeHelper.getBitmap(this, R.drawable.smoke03);
		_noroshiImages.put("smoke03", ExtraLayout.resizeBaseBitmap(this, smoke03));
		Bitmap smoke04 = BitmapDecodeHelper.getBitmap(this, R.drawable.smoke04);
		_noroshiImages.put("smoke04", ExtraLayout.resizeBaseBitmap(this, smoke04));
		Bitmap smoke05 = BitmapDecodeHelper.getBitmap(this, R.drawable.smoke05);
		_noroshiImages.put("smoke05", ExtraLayout.resizeBaseBitmap(this, smoke05));

		ActiveRecordPartial arp = new ActiveRecordPartial(this, MigrationSettings.NOROSHIES_TABLE);
		int c=arp.destroy_all();
		Log.d(TAG, "c:"+c);
		HashMap<String,Object> cv = new HashMap<String,Object>();
		cv.put("user_id", 1);
		cv.put("latitude", 35.662836);
		cv.put("longitude", 139.731443);
		cv.put("elevation", 30.9146842956543);
		cv.put("image_id", 1);
		cv.put("message", "六本木駅");
		cv.put("put_up_datetime", System.currentTimeMillis());
		cv.put("updated_at", System.currentTimeMillis());
		cv.put("created_at", System.currentTimeMillis());
		arp.create(cv);
		HashMap<String,Object> cv2 = new HashMap<String,Object>();
		cv2.put("user_id", 2);
		cv2.put("latitude", 35.659337);
		cv2.put("longitude", 139.731701);
		cv2.put("elevation", 15.37118148803711);
		cv2.put("image_id", 2);
		cv2.put("message", "テレビ朝日");
		cv2.put("put_up_datetime", System.currentTimeMillis());
		cv2.put("updated_at", System.currentTimeMillis());
		cv2.put("created_at", System.currentTimeMillis());
		arp.create(cv2);

		HashMap<String,Object> cv3 = new HashMap<String,Object>();
		cv3.put("user_id", 3);
		cv3.put("latitude", 35.655575);
		cv3.put("longitude", 139.702876);
		cv3.put("elevation", 22.04828834533691);
		cv3.put("image_id", 4);
		cv3.put("message", "スマートニュース本社");
		cv3.put("put_up_datetime", System.currentTimeMillis());
		cv3.put("updated_at", System.currentTimeMillis());
		cv3.put("created_at", System.currentTimeMillis());
		arp.create(cv3);
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	protected void onResume(){
		super.onResume();
		_camera = setupCamera(_cameraId);
		setupPanorama(_camera.getParameters());
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
		Location defaultLocation = _locationManager.getLastKnownLocation(sp.getString(this.getString(R.string.usingProviderKey), LocationManager.NETWORK_PROVIDER));
		if(defaultLocation != null){
			requestGoogleAPI(defaultLocation);
		}
		// 位置情報の更新を受け取るように設定
		_locationManager.requestLocationUpdates(usableBestProvider(), // プロバイダ
					0, // 通知のための最小時間間隔
					0, // 通知のための最小距離間隔
					_locationListener); // 位置情報リスナー
		//加速度センサー
		_sensorManager.registerListener(_sensorEventListener, _sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_UI);
		//地磁気センサー
		_sensorManager.registerListener(_sensorEventListener, _sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_UI);
	};

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private String usableBestProvider(){
		String alloedProviders = Secure.getString(getContentResolver(), Secure.LOCATION_PROVIDERS_ALLOWED);
		Log.d(TAG, "provider:"+alloedProviders);
		if(alloedProviders.contains(LocationManager.GPS_PROVIDER)){
			return LocationManager.GPS_PROVIDER;
		}else{
			//Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			//this.startActivity(intent);
			return LocationManager.NETWORK_PROVIDER;
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private Camera setupCamera(int cameraId){
		SurfaceView preview = (SurfaceView) findViewById(R.id.CameraPreview);
		SurfaceHolder holder = preview.getHolder();
		holder.addCallback(new SurfaceHolder.Callback() {

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				releaseCamera();
			}

			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				if(_camera !=  null){
					try {
						_camera.setPreviewDisplay(holder);
					} catch (IOException exception) {
						releaseCamera();
					}
				}
			}

			@Override
			public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
				if(_camera !=  null){
					try {
						_camera.setPreviewDisplay(holder);
					} catch (IOException exception) {
						releaseCamera();
					}
				}
			}
		});
		if(Build.VERSION.SDK_INT < 11){
			holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}
		Camera camera = Camera.open();
		try {
			camera.setPreviewDisplay(holder);
		} catch (Exception e) {
			e.printStackTrace();
			return camera;
		}
		camera.stopPreview();
		camera.setDisplayOrientation(Tools.getCameraDisplayOrientation(this, cameraId));
		camera.startPreview();
		return camera;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private void setupPanorama(Camera.Parameters cp){
		//のろし画像追加
		Rect displayRect = ExtraLayout.getDisplaySize(this);
		ArrayList<ActiveRecordPartialInstance> dataList = getNoroshiDataList();
		for(int i = 0;i < dataList.size();++i){
			ActiveRecordPartialInstance data = dataList.get(i);
			HashMap<String, Panorama> panoramaList = new HashMap<String, Panorama>();
			switch(data.getInt("image_id")){
				case 1:
					panoramaList.put("noroshi", new Panorama(_noroshiImages.get("noroshi50mb"), displayRect.width(), displayRect.height(), cp.getHorizontalViewAngle(), cp.getVerticalViewAngle()));
					break;
				case 2:
					panoramaList.put("noroshi", new Panorama(_noroshiImages.get("noroshi50mp"), displayRect.width(), displayRect.height(), cp.getHorizontalViewAngle(), cp.getVerticalViewAngle()));
					break;
				case 3:
					panoramaList.put("noroshi", new Panorama(_noroshiImages.get("noroshi50mw"), displayRect.width(), displayRect.height(), cp.getHorizontalViewAngle(), cp.getVerticalViewAngle()));
					break;
				case 4:
					panoramaList.put("noroshi", new Panorama(_noroshiImages.get("noroshi50my"), displayRect.width(), displayRect.height(), cp.getHorizontalViewAngle(), cp.getVerticalViewAngle()));
					break;
			}
			//panoramaList.put("smoke02", new Panorama(_noroshiImages.get("smoke02"), displayRect.width(), displayRect.height(), cp));
			//panoramaList.put("smoke03", new Panorama(_noroshiImages.get("smoke03"), displayRect.width(), displayRect.height(), cp));
			//panoramaList.put("smoke04", new Panorama(_noroshiImages.get("smoke04"), displayRect.width(), displayRect.height(), cp));
			//panoramaList.put("smoke05", new Panorama(_noroshiImages.get("smoke05"), displayRect.width(), displayRect.height(), cp));
			_cameraOverlayView.putPanoramaImageSet(data.getString("user_id"), new PanoramaImageSet(panoramaList));
			_cameraOverlayView.setTargetLocation(data.getString("user_id"), data.getDouble("latitude") ,data.getDouble("longitude"),data.getDouble("elevation"));
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	protected void onPause(){
		super.onPause();
		_sensorManager.unregisterListener(_sensorEventListener);
		_locationManager.removeUpdates(_locationListener);
		//カメラを切る
		this.releaseCamera();
		_cameraOverlayView.relaesePanoramas();
	};

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private void releaseCamera(){
		if (_camera != null){
			_camera.cancelAutoFocus();
			_camera.stopPreview();
			_camera.setPreviewCallback(null);
			_camera.release();
			_camera = null;
		};
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	protected void onDestroy() {
		super.onDestroy();
		_cameraOverlayView.releaseAllImage();
		_locationManager = null;
		for(Entry<String, Bitmap> e : _noroshiImages.entrySet()) {
			e.getValue().recycle();
		}
		_noroshiImages.clear();
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = sp.edit();
		editor.remove(this.getString(R.string.usingProviderKey));
		editor.commit();
	}

	//--------------------------------------------------------------------------------------------------------------------------------------------------------------

	private ArrayList<ActiveRecordPartialInstance> getNoroshiDataList(){
		ActiveRecordPartial noroshies = new ActiveRecordPartial(this, MigrationSettings.NOROSHIES_TABLE);
		Calendar limitTime = Calendar.getInstance();
		limitTime.add(Calendar.MINUTE, -TIME_LIMIT_MINUTE);
		ArrayList<ActiveRecordPartialInstance> dataList = noroshies.all();
		return dataList;
	}

	//--------------------------------------------------------------------------------------------------------------------------------------------------------------

	private void requestGoogleAPI(Location location){
		Bundle requestParams = new Bundle();
		requestParams.putBoolean("sensor", true);
		requestParams.putString("locations", location.getLatitude() + "," + location.getLongitude());
		String url = Config.GOOGLEMAPAPI_ELEVATION_TO_JSON_URL + "?" + Tools.makeUrlParams(requestParams);
		Log.d(TAG, "provider:"+location.getProvider());
		AsynkWebAPIRequest request = new AsynkWebAPIRequest(new RequestFinishCallback() {
			@Override
			public void complete(String json) {
				Log.d(TAG, json);
				try {
					JSONObject data = new JSONObject(json);
					JSONArray result = data.getJSONArray("results");
					JSONObject first = result.getJSONObject(0);
					double elevation = first.getDouble("elevation");
					JSONObject location = first.getJSONObject("location");
					_cameraOverlayView.setDeviceLocation(location.getDouble("lat"), location.getDouble("lng"), elevation);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void cancel() {

			}

			@Override
			public void error(int errorCode) {

			}
		});
		request.execute(url);
	}
}
