package jp.co.noroshi;

import java.util.Map;
import java.util.Map.Entry;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import android.location.Location;
import android.util.Log;

public class PanoramaImageSet{

	private static final String TAG = "noroshi_PanoramaGroup";

	private Map<String, Panorama> _panoramaList;
	private int _alphaCache = 0;

	public PanoramaImageSet(Map<String, Panorama> panoramaList) {
		_panoramaList = panoramaList;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void setTargetLocation(double latitude, double longitude, double elevation){
		for(Entry<String, Panorama> e : _panoramaList.entrySet()) {
			e.getValue().setTargetLocation(latitude, longitude, elevation);
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void setDeviceLocation(double latitude, double longitude, double elevation){
		for(Entry<String, Panorama> e : _panoramaList.entrySet()) {
			e.getValue().setDeviceLocation(latitude, longitude, elevation);
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void setDeviceOrientation(double pitch, double roll, double azimuth){
		for(Entry<String, Panorama> e : _panoramaList.entrySet()) {
			e.getValue().setDeviceOrientation(pitch, roll, azimuth);
		}
		//setupLiveCoalsEffect(pitch, roll, azimuth);
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void render(Canvas canvas){
		for(Entry<String, Panorama> e : _panoramaList.entrySet()) {
			e.getValue().render(canvas);
		}
	}

	private void setupLiveCoalsEffect(double pitch, double roll, double azimuth){
		Panorama base = _panoramaList.get("noroshi");
		base.setDeviceOrientation(pitch, roll, azimuth);
		PointF basePosition = base.getPositionF();
		Panorama liveCoal = _panoramaList.get("smoke05");
		liveCoal.setDeviceOrientation(pitch, roll, azimuth);
		liveCoal.setPosition(basePosition.x + base.getImageWidth() / 2, basePosition.y + base.getImageHeight() + liveCoal.getImageHeight() / 2);
		Log.d(TAG,"X:"+liveCoal.getPositionF().x +" Y:"+liveCoal.getPositionF().y);
		Paint paint = new Paint();
		_alphaCache += 10;
		_alphaCache = _alphaCache % 360;
		int alpha = (int)(Math.sin(Math.toRadians(_alphaCache)) * 64);
		Log.d(TAG, "alpha:"+alpha);
		paint.setAlpha(192 + alpha);
		liveCoal.setPaint(paint);
	}
	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void release(){
		for(Entry<String, Panorama> e : _panoramaList.entrySet()) {
			e.getValue().release();
		}
		_panoramaList.clear();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------
}