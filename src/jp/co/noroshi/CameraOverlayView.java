package jp.co.noroshi;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.xml.sax.helpers.LocatorImpl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.location.Location;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.View;

public class CameraOverlayView extends View {

	private static final String TAG = "noroshi_CameraOverrideView";
	private Bitmap _clearImage = null;
	private Bitmap _renderBaseImage = null;
	private HashMap<String, PanoramaImageSet> _panoramaLayerList;

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public CameraOverlayView(Context context, AttributeSet attrs) {
		super(context, attrs);
		_panoramaLayerList = new HashMap<String, PanoramaImageSet>();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(_renderBaseImage != null){
			_renderBaseImage.recycle();
			_renderBaseImage = null;
		}
		_renderBaseImage = _clearImage.copy(Bitmap.Config.ARGB_8888, true);
		Canvas bitmapCanvas = new Canvas(_renderBaseImage);
		for(Entry<String, PanoramaImageSet> e : _panoramaLayerList.entrySet()) {
			e.getValue().render(bitmapCanvas);
		}
		canvas.drawBitmap(_renderBaseImage, null, new Rect(0,0,_renderBaseImage.getWidth(), _renderBaseImage.getHeight()), null);
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		if(_clearImage != null){
			_clearImage.recycle();
			_clearImage = null;
		}
		_clearImage = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		this.invalidate();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void putPanoramaImageSet(String key,PanoramaImageSet panoramaImageSet){
		_panoramaLayerList.put(key, panoramaImageSet);
		this.invalidate();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void putPanoramaImageSetList(HashMap<String, PanoramaImageSet> panoramaImageSetList){
		_panoramaLayerList.putAll(panoramaImageSetList);
		this.invalidate();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void setDeviceOrientaion(double pitch, double roll, double azimuth){
		for(Entry<String, PanoramaImageSet> e : _panoramaLayerList.entrySet()) {
			e.getValue().setDeviceOrientation(pitch, roll, azimuth);;
		}
		this.invalidate();
	}

	public void setDeviceLocation(double latitude , double longitude, double elevation){
		Log.d(TAG, ""+longitude + " " + latitude+ " " + elevation);
		//経度:longitude, 緯度:latitude, 標高:elevation
		for(Entry<String, PanoramaImageSet> e : _panoramaLayerList.entrySet()) {
			e.getValue().setDeviceLocation(latitude, longitude, elevation);
		}
		this.invalidate();
	}

	public void setTargetLocation(String key, double latitude , double longitude, double elevation){
		_panoramaLayerList.get(key).setTargetLocation(latitude, longitude, elevation);
		this.invalidate();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void relaesePanoramas(){
		for(Entry<String, PanoramaImageSet> e : _panoramaLayerList.entrySet()) {
			e.getValue().release();
		}
		_panoramaLayerList.clear();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void releaseAllImage(){
		relaesePanoramas();
		if(_clearImage != null){
			_clearImage.recycle();
			_clearImage = null;
		}
		if(_renderBaseImage != null){
			_renderBaseImage.recycle();
			_renderBaseImage = null;
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------
}