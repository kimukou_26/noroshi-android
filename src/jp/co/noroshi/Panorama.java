package jp.co.noroshi;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import android.location.Location;
import android.util.Log;

public class Panorama extends OverlayImage{

	private static final String TAG = "noroshi_Panorama";
	//この距離以上離れると画像が小さくならない(単位はメートル)
	private static final double FARTHEST_LIMIT_METER = 10000;
	//この距離以上近づくと画像が大きくならない(単位はメートル)
	private static final double NEAREST_LIMIT_METER = 50;
	private float _horizontalViewAngle = 0;
	private float _verticalViewAngle = 0;

	private double _targetLatitude = 0;
	private double _targetLongitude = 0;
	private double _targetElevation = 0;
	private double _deviceLatitude = 0;
	private double _deviceLongitude = 0;
	private double _deviceElevation = 0;
	private int _viewWidth = 0;
	private int _viewHeight = 0;

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	//経度:longitude, 緯度:latitude, 標高:elevation
	public Panorama(Bitmap image, int viewWidth, int viewHeight, float cameraHorizontalViewAngle, float cameraVerticalViewAngle) {
		super(image);
		_viewWidth = viewWidth;
		_viewHeight = viewHeight;
		//水平方向に見えるカメラの領域
		_horizontalViewAngle = cameraHorizontalViewAngle;
		
		//垂直方向に見えるカメラの領域
		_verticalViewAngle = cameraVerticalViewAngle;

		//とりあえず検証用のため
		//_deviceLatitude = 35.6721082;
		//_deviceLongitude = 139.4546881;
		//_deviceElevation = 65.49675750732422;
		//会場の場所
		_deviceLatitude = 35.6604;
		_deviceLongitude = 139.729043;
		_deviceElevation = 28.34195518493652;


		//とりあえずデフォルトは渋谷駅の場所
		//_targetLatitude = 35.658517;
		//_targetLongitude = 139.701334;
		//_targetElevation = 15.43;
		//西府駅の場所
		//_targetLatitude = 35.670967;
		//_targetLongitude = 139.45739900000001;
		//_targetElevation = 63.34;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void setTargetLocation(double latitude, double longitude, double elevation){
		Log.d(TAG, "targetLat:"+latitude+" targetLon:"+longitude+" targetEle:"+elevation);
		_targetLatitude = latitude;
		_targetLongitude = longitude;
		_targetElevation = elevation;
		this.setPanoramaScale();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void setDeviceLocation(double latitude, double longitude, double elevation){
		Log.d(TAG, "deviceLat:"+latitude+" deviceLon:"+longitude+" deviceEle:"+elevation);
		_deviceLatitude = latitude;
		_deviceLongitude = longitude;
		_deviceElevation = elevation;
		this.setPanoramaScale();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void setDeviceOrientation(double pitch, double roll, double azimuth){
		double azimuthAngle = calcAzimuthAngleDegree();
		double inclinationAngle = calcInclinationAngleDegree();
		double theta = azimuth - azimuthAngle;
		//Androidは基本カメラは横向きなので、横=垂直と考えるべき
		float viewAngleX =  (_verticalViewAngle / 2) - (float)theta;
		float positionX = viewAngleX * _viewWidth / _verticalViewAngle;

		//Androidは基本カメラは横向きなので、縦=水平と考えるべき
		//端末を上に向けるとpitchの値は減っていくので+にする
		double phi = inclinationAngle + pitch;
		float viewAngleY = (_horizontalViewAngle / 2) - (float)phi;
		float positionY = viewAngleY * _viewHeight / _horizontalViewAngle;
		this.setPosition(positionX, positionY);
		this.setPanoramaScale();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private double calcInclinationAngleDegree(){
		//自分から見た相手の高さ
		double differenceElevationMeter = _targetElevation - _deviceElevation;
		double distance = calcDistance();
		return Math.toDegrees(Math.atan(differenceElevationMeter/distance));
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private void setPanoramaScale(){
		double distance = calcDistance();
		if(NEAREST_LIMIT_METER < distance && distance < FARTHEST_LIMIT_METER){
			float scale = (float)(1.0f - ((distance - NEAREST_LIMIT_METER ) * 0.9f / (FARTHEST_LIMIT_METER - NEAREST_LIMIT_METER)));
			this.setScale(scale, scale);
		}else if(FARTHEST_LIMIT_METER < distance){
			this.setScale(0.1f, 0.1f);
		}else{
			this.setScale(1.0f,1.0f);
		}
	}

	private double calcAzimuthAngleDegree(){
		//参考:http://kawae.dyndns.org/xt/modules/xpwiki/?%E6%96%B9%E4%BD%8D%E8%A7%92%E3%81%AE%E8%A8%88%E7%AE%97%E5%BC%8F
		//地点(B1,L1)から地点(B2,L2)を見た場合の方位角φと距離dの計算式
		//φ=atan2(cos(B2) * sin(L2 - L1) , cos(B1) * sin(B2) - sin(B1) * cos(B2) * cos(L2 - L1)
		//B(緯度)は北緯が正値, 南緯は負値
		//L(経度)は東経が正値, 西経は負値
		double targetLatitudeRad = Math.toRadians(_targetLatitude);
		double differentialLongitudeRad = Math.toRadians(_targetLongitude - _deviceLongitude);
		double deviceLatitudeRad = Math.toRadians(_deviceLatitude);

		double theta =Math.atan2(Math.cos(targetLatitudeRad) * Math.sin(differentialLongitudeRad), Math.cos(deviceLatitudeRad) * Math.sin(targetLatitudeRad) - Math.sin(deviceLatitudeRad) * Math.cos(targetLatitudeRad) * Math.cos(differentialLongitudeRad));
		//double theta = 90 - Math.atan2(Math.sin(_targetLongitude - _deviceLongitude), Math.cos(_deviceLatitude) * Math.tan(_targetLatitude) - Math.sin(_deviceLatitude) * Math.cos(_targetLongitude - _deviceLongitude));
		//Log.d(TAG,"Tlat:"+_targetLatitude+" Tlon:"+_targetLongitude+" Dlat:"+_deviceLatitude+" Dlon:"+_deviceLongitude);
		//Log.d(TAG,"theta:"+Math.toDegrees(theta));
		return Math.toDegrees(theta);
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	/*
	 *参考:http://web-terminal.blogspot.jp/2013/01/iphone.html
	 *参考:http://h2caster.net/home/gpsandpda/hybeny.php
		ヒュベニの距離計算式
		D=sqt((M*dP)^2+(N*cos(P)*dR)^2)
		D: ２点間の距離（弧）(m)
		P: ２点の平均緯度P[ラジアン]=(P1+P2)/2
		dP: ２点の緯度差dP[ラジアン]=P1-P2
		dR: ２点の経度差dR[ラジアン]=R1-R2
		M: 子午線曲率半径 M=6334834/sqrt((1-0.006674*sin(P)^2)^3)
		N: 卯酉線曲率半径 N=6377397/sqrt(1-0.006674*sin(P)^2)
	*/
	private double calcDistance(){
		double P = (_deviceLatitude + _targetLatitude) / 2 * Math.PI / 180;
		double dP = (_deviceLatitude - _targetLatitude) * Math.PI / 180;
		double dR = (_deviceLongitude - _targetLongitude) * Math.PI / 180;
		double M = 6334834 / Math.sqrt(Math.pow(1 - 0.006674 * Math.sin(P) * Math.sin(P), 3));
		double N = 6377397 / Math.sqrt(1 - 0.006674 * Math.sin(P) * Math.sin(P));
		double D = Math.sqrt((M * dP) * (M * dP) + (N * Math.cos(P) * dR) * (N * Math.cos(P) * dR));
		return D;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------
}