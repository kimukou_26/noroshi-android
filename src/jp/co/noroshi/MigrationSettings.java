package jp.co.noroshi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class MigrationSettings{

	//DBのバージョン番号、これを変えると、upgradeが走る
	public static final int DBVERSION = 1;
	public static final String NOROSHIES_TABLE = "noroshies";
	public static final String USERS_TABLE = "users";

	public static ArrayList<String> createTable() {
		ArrayList<String> SQLList = new ArrayList<String>();
		HashMap<String, String[]> tableDatas = tableSettings();
		for(Entry<String, String[]> e : tableDatas.entrySet()) {
			String SQL = "create table "+ e.getKey() +"(";
			String[] columnList = e.getValue();
			for(int i = 0;i < columnList.length; ++i){
				SQL = SQL + columnList[i];
				if(i != columnList.length - 1){
					SQL = SQL + ",";
				}
			}
			SQL = SQL + ");";
			SQLList.add(SQL);
		}
		return SQLList;
	}

	//ここに定数を打ち込んでいく
	private static HashMap<String, String[]> tableSettings(){
		HashMap<String, String[]> tableData = new HashMap<String, String[]>();
		//TODO nameは後でuserIdに変わる予定
		String[] columns1 = {
			"id INTEGER PRIMARY KEY AUTOINCREMENT",
			"name TEXT",
			"device_token TEXT",
			"updated_at integer NOT NULL",
			"created_at integer NOT NULL"
		};
		String[] columns2 = {
			"id INTEGER PRIMARY KEY AUTOINCREMENT",
			"user_id INTEGER NOT NULL",
			"latitude REAL NOT NULL",
			"longitude REAL NOT NULL",
			"elevation REAL NOT NULL",
			"image_id INTEGER NOT NULL default 1",
			"message TEXT",
			"put_up_datetime INTEGER NOT NULL",
			"updated_at integer NOT NULL",
			"created_at integer NOT NULL"
		};
		tableData.put(USERS_TABLE, columns1);
		tableData.put(NOROSHIES_TABLE, columns2);
		return tableData;
	}

	public static ArrayList<String> createIndex(){
		ArrayList<String> newIndex = new ArrayList<String>();
		String indexQuery1 = "CREATE INDEX "+ USERS_TABLE + "_device_token " + "ON "+ USERS_TABLE +" (device_token)";
		String indexQuery2 = "CREATE INDEX "+ NOROSHIES_TABLE + "_user_id " + "ON "+ NOROSHIES_TABLE +" (user_id)";
		String indexQuery3 = "CREATE INDEX "+ NOROSHIES_TABLE + "_put_up_datetime " + "ON "+ NOROSHIES_TABLE +" (put_up_datetime)";
		newIndex.add(indexQuery1);
		newIndex.add(indexQuery2);
		newIndex.add(indexQuery3);
		return newIndex;
	}

}
