package jp.co.noroshi;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.EventListener;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;

public class AsynkWebAPIRequest extends AsyncTask<String, Void, String> {

	private int _statusCode = Config.UNKOWN_ERROR_STATUS_CODE;
	private boolean _isError = false;
	private RequestFinishCallback _callback;

	public AsynkWebAPIRequest(RequestFinishCallback callback) {
		_callback = callback;
	}

	@Override
	protected String doInBackground(String... url) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpUriRequest httpRequest = new HttpGet(url[0]);
		HttpResponse httpResponse = null;
		String returnData = null;
		try {
			httpResponse = httpClient.execute(httpRequest);
			if (httpResponse != null && httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				httpResponse.getEntity().writeTo(outputStream);
				returnData = outputStream.toString(); // JSONデータ
			}else{
				if(httpResponse != null){
					_statusCode = httpResponse.getStatusLine().getStatusCode();
				}
				_isError = true;
				cancel(true);
			}
		} catch (ClientProtocolException e) {
			_isError = true;
			e.printStackTrace();
		} catch (IOException e) {
			_isError = true;
			e.printStackTrace();
		}finally{
			httpClient.getConnectionManager().shutdown();
		}
		return returnData;
	}

	@Override
	protected void onPostExecute(String result) {
		_callback.complete(result);
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
		if(_isError){
			_callback.error(_statusCode);
		}else{
			_callback.cancel();
		}
	}

	public interface RequestFinishCallback extends EventListener {
		public void complete(String json);
		public void cancel();
		public void error(int errorCode);
	}
}
