package jp.co.noroshi;

import java.io.File;

import jp.co.noroshi.FacebookAction.LoginResultListener;
import jp.co.noroshi.TwitterAction.OAuthResultListener;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnCancelListener;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PutUpNoroshiActivity extends Activity {

	private ImageButton _twitterButton;
	private WebView _twitterWebView;
	private TwitterAction _twitterAction;
	private FacebookAction _facebookAction;
	private ProgressDialog _sendingImageDialog;
	private TextView _textCount;
	private String _tweetString = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(ExtraLayout.getParenetView(this, R.layout.put_up_noroshi));
		ImageView PutUpBackgroundImage = (ImageView) findViewById(R.id.PutUpBackgroundImage);
		PutUpBackgroundImage.setImageResource(R.drawable.doneand01);

		ImageButton FacebookButton = (ImageButton) findViewById(R.id.FaceBookButton);
		ExtraLayout.setBaseImageView(this, FacebookButton, R.drawable.doneand02off);
		FacebookButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setupFacebook();
			}
		});
		FacebookButton.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				ImageButton image = (ImageButton) v;
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					image.setImageResource(R.drawable.doneand02on);
					break;
				case MotionEvent.ACTION_CANCEL:
					image.setImageResource(R.drawable.doneand02off);
					break;
				case MotionEvent.ACTION_UP:
					image.setImageResource(R.drawable.doneand02off);
					break;
				case MotionEvent.ACTION_OUTSIDE:
					image.setImageResource(R.drawable.doneand02off);
					break;
				}
				return false;
			}
		});

		_twitterButton =  (ImageButton) findViewById(R.id.TwitterButton);
		ExtraLayout.setBaseImageView(this, _twitterButton, R.drawable.doneand03off);
		_twitterButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setupTweet();
			}
		});
		_twitterButton.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				ImageButton image = (ImageButton) v;
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					image.setImageResource(R.drawable.doneand03on);
					break;
				case MotionEvent.ACTION_CANCEL:
					image.setImageResource(R.drawable.doneand03off);
					break;
				case MotionEvent.ACTION_UP:
					image.setImageResource(R.drawable.doneand03off);
					break;
				case MotionEvent.ACTION_OUTSIDE:
					image.setImageResource(R.drawable.doneand03off);
					break;
				}
				return false;
			}
		});

		ImageButton ConfirmBeaconButton = (ImageButton) findViewById(R.id.ConfirmBeaconButton);
		ExtraLayout.setBaseImageView(this, ConfirmBeaconButton, R.drawable.doneand04off);
		ConfirmBeaconButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
			}
		});
		ConfirmBeaconButton.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				ImageButton image = (ImageButton) v;
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					image.setImageResource(R.drawable.doneand04on);
					break;
				case MotionEvent.ACTION_CANCEL:
					image.setImageResource(R.drawable.doneand04off);
					break;
				case MotionEvent.ACTION_UP:
					image.setImageResource(R.drawable.doneand04off);
					break;
				case MotionEvent.ACTION_OUTSIDE:
					image.setImageResource(R.drawable.doneand04off);
					break;
				}
				return false;
			}
		});

		ImageButton SearchBeaconButton = (ImageButton) findViewById(R.id.SearchBeaconButton);
		ExtraLayout.setBaseImageView(this, SearchBeaconButton, R.drawable.doneand05off);
		SearchBeaconButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent cameraIntent = new Intent(PutUpNoroshiActivity.this, CameraActivity.class);
				startActivity(cameraIntent);
			}
		});
		SearchBeaconButton.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				ImageButton image = (ImageButton) v;
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					image.setImageResource(R.drawable.doneand05on);
					break;
				case MotionEvent.ACTION_CANCEL:
					image.setImageResource(R.drawable.doneand05off);
					break;
				case MotionEvent.ACTION_UP:
					image.setImageResource(R.drawable.doneand05off);
					break;
				case MotionEvent.ACTION_OUTSIDE:
					image.setImageResource(R.drawable.doneand05off);
					break;
				}
				return false;
			}
		});

		_twitterWebView = (WebView) findViewById(R.id.TwitterWebView);
		_twitterWebView.getSettings().setJavaScriptEnabled(true);

		_twitterWebView.setWebChromeClient(new WebChromeClient(){
			//WebViewがURLを読み込んでいる最中の処理の設定
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				super.onProgressChanged(view, newProgress);
				setProgress(newProgress * 100);
			}
		});
		_twitterWebView.setWebViewClient(new WebViewClient(){
			//※Android2.*系でも常時呼ばれる
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				if ((url != null) && (url.startsWith(Config.TWITTER_CALLBACKURL))) {
					_twitterWebView.stopLoading();
					_twitterWebView.setVisibility(View.INVISIBLE);
					//認証完了後にCallbackするURLをフックし、AccessTokenを取得する処理を行う
					_twitterAction.returnOAuth(url);
				}
			}
		});
		_twitterWebView.setVisibility(View.INVISIBLE);

		_twitterAction = new TwitterAction(this);
		_twitterAction.setOnUploadListener(new TwitterAction.UploadListener() {

			@Override
			public void success(String Tweet) {
				_sendingImageDialog.dismiss();
				Tools.showToast(PutUpNoroshiActivity.this, PutUpNoroshiActivity.this.getString(R.string.ContributeSucessMessage));
			}

			@Override
			public void error(int StatusCode) {
				_sendingImageDialog.dismiss();
				Tools.showToast(PutUpNoroshiActivity.this, PutUpNoroshiActivity.this.getString(R.string.ContributeFailedMessage));
			}
		});

		_facebookAction = new FacebookAction(this, savedInstanceState);
		_facebookAction.setOnUploadListener(new FacebookAction.UploadListener() {
			@Override
			public void success() {
				_sendingImageDialog.dismiss();
				Tools.showToast(PutUpNoroshiActivity.this, PutUpNoroshiActivity.this.getString(R.string.ContributeSucessMessage));
			}

			@Override
			public void error() {
				_sendingImageDialog.dismiss();
				Tools.showToast(PutUpNoroshiActivity.this, PutUpNoroshiActivity.this.getString(R.string.ContributeFailedMessage));
			}
		});
		_sendingImageDialog = new ProgressDialog(this);
		_sendingImageDialog.setCancelable(true);
		_sendingImageDialog.setMessage(this.getString(R.string.UploadingImageMessage));
		_sendingImageDialog.setIndeterminate(true);
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private void showTweetDialog(){
		final Dialog tweetDialog = new Dialog(this);
		tweetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		tweetDialog.setCancelable(true);
		tweetDialog.show();
		tweetDialog.setContentView(R.layout.tweetdialog);
		_textCount = (TextView) tweetDialog.findViewById(R.id.TweetCountText);
		int textCount = Config.TWITTER_MAX_TEXT_COUNT - _tweetString.length();
		_textCount.setText(String.valueOf(textCount));
		if(textCount >= 0){
			_textCount.setTextColor(Color.BLACK);
		}else{
			_textCount.setTextColor(Color.RED);
		}
		final EditText tweetText = (EditText) tweetDialog.findViewById(R.id.TweetText);
		tweetText.setText(_tweetString);
		tweetText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				int remainTextCount = Config.TWITTER_MAX_TEXT_COUNT - s.length();
				if(remainTextCount >= 0){
					_textCount.setTextColor(Color.BLACK);
				}else{
					_textCount.setTextColor(Color.RED);
				}
				_textCount.setText(String.valueOf(remainTextCount));
				_tweetString = s.toString();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
		Button sendTweetButton = (Button) tweetDialog.findViewById(R.id.SendTweetButton);
		sendTweetButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String tweet = tweetText.getText().toString();
				if((Config.TWITTER_MAX_TEXT_COUNT - tweet.length()) >= 0){
					sendTwitterAction(tweet);
					//ツイートボタンを押したら出ているキーボードは消えてもらう
					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
					tweetDialog.cancel();
				}else{
					Tools.showToast(PutUpNoroshiActivity.this, PutUpNoroshiActivity.this.getString(R.string.TweetOverMessage));
				}
			}
		});
		tweetDialog.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_HIDDEN);
			}
		});
	}

	private void showFacebookDialog(){
		final Dialog tweetDialog = new Dialog(this);
		tweetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		tweetDialog.setCancelable(true);
		tweetDialog.show();
		tweetDialog.setContentView(R.layout.tweetdialog);
		TextView textcount = (TextView) tweetDialog.findViewById(R.id.TweetCountText);
		textcount.setVisibility(View.GONE);
		final EditText tweetText = (EditText) tweetDialog.findViewById(R.id.TweetText);
		tweetText.setText(_tweetString);
		Button sendTweetButton = (Button) tweetDialog.findViewById(R.id.SendTweetButton);
		sendTweetButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String tweet = tweetText.getText().toString();
				sendFacebookAction(tweet);
				//ツイートボタンを押したら出ているキーボードは消えてもらう
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				tweetDialog.cancel();
			}
		});
		tweetDialog.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_HIDDEN);
			}
		});
	}
	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private void setupTweet(){
		SharedPreferences setting = PreferenceManager.getDefaultSharedPreferences(this);
		String AccessToken = setting.getString(this.getString(R.string.TwitterAccessTokenKey), null);
		String AccessTokenSecret = setting.getString(this.getString(R.string.TwitterAccessTokenSecretKey), null);
		if(AccessToken != null && AccessTokenSecret != null){
			_twitterAction.setAccessToken(AccessToken, AccessTokenSecret);
			showTweetDialog();
		}else{
			if(Tools.checkNetWork(this)){
				_twitterAction.setOnOAuthResultListener(new OAuthResultListener() {
					//認証ページのURLを取得した時に呼ばれる
					@Override
					public void requestOAuthUrl(String url) {
						if(url != null){
							_twitterWebView.loadUrl(url);
							_twitterWebView.setVisibility(View.VISIBLE);
							//WebView上で入力時にキーボードを出現させるためにフォーカスをあてる。
							_twitterWebView.requestFocus();
						}
					}

					//認証完了後AccessToken取得完了した時に呼ばれる
					@Override
					public void oAuthResult(String token, String tokenSecret) {
						_twitterButton.setClickable(true);
						_twitterWebView.setVisibility(View.INVISIBLE);
						_twitterAction.setAccessToken(token, tokenSecret);
						showTweetDialog();
					}

					//認証エラーが発生した時に呼ばれる
					@Override
					public void oAuthError(int StatusCode) {
						_twitterButton.setClickable(true);
						_twitterWebView.setVisibility(View.INVISIBLE);
					}
				});
				_twitterButton.setClickable(false);
				_twitterAction.startOAuth();
			}else{
				Tools.showToast(this, this.getString(R.string.CannotAccedssNetwprkMessage));
			}
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(_twitterWebView.getVisibility() == View.VISIBLE && keyCode == KeyEvent.KEYCODE_BACK){
			_twitterWebView.setVisibility(View.INVISIBLE);
			_twitterButton.setClickable(true);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private void sendTwitterAction(String tweet){
		if(Tools.checkNetWork(this)){
			_sendingImageDialog.show();
			//Fileは投稿する画像のファイル、第二引数(String)はツイート文
			_twitterAction.sendTweetToTwitter(tweet);
		}else{
			Tools.showToast(this, this.getString(R.string.CannotAccedssNetwprkMessage));
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private void setupFacebook(){
		if(_facebookAction.isLogin()){
			showFacebookDialog();
		}else{
			if(Tools.checkNetWork(this)){
				_facebookAction.startLogin();
				_facebookAction.setOnLoginResultListener(new LoginResultListener() {

					@Override
					public void success(String accessToken) {
						showFacebookDialog();
					}

					@Override
					public void error() {
						Tools.showToast(PutUpNoroshiActivity.this, PutUpNoroshiActivity.this.getString(R.string.AuthorizationFailedMessage));
					}

					@Override
					public void cancel() {
						Tools.showToast(PutUpNoroshiActivity.this, PutUpNoroshiActivity.this.getString(R.string.AuthorizationCancelMessage));
					}
				});
			}else{
				Tools.showToast(this, this.getString(R.string.CannotAccedssNetwprkMessage));
			}
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private void sendFacebookAction(String message){
		if(Tools.checkNetWork(this)){
			_sendingImageDialog.show();
			_facebookAction.uploadMessage(message);
		}else{
			Tools.showToast(this, this.getString(R.string.CannotAccedssNetwprkMessage));
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		_facebookAction.setLoginResult(requestCode, resultCode, data);
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Tools.releaseImageView((ImageButton) findViewById(R.id.FaceBookButton));
		Tools.releaseImageView(_twitterButton);
		Tools.releaseImageView((ImageButton) findViewById(R.id.ConfirmBeaconButton));
		Tools.releaseImageView((ImageButton) findViewById(R.id.SearchBeaconButton));
		Tools.releaseImageView((ImageView) findViewById(R.id.PutUpBackgroundImage));
		Tools.releaseWebView(_twitterWebView);
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------
}
