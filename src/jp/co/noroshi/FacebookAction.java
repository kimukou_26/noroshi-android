package jp.co.noroshi;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.EventListener;

import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.TokenCachingStrategy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


//Facebookへの認証処理
public class FacebookAction{

	private static final String TAG = "noroshi_FacebookAction";
	private static final String APP_ID = Config.FACEBOOK_APP_ID;
	private static final String[] PERMISSIONS = Config.FACEBOOK_PERMISSIONS;
	private Activity _activity;
	private Session _session;
	private UploadListener _uploadListener = null;
	private LoginResultListener _loginResultListener = null;

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public FacebookAction(Activity act, Bundle savedInstanceState){
		_activity = act;
		_session = Session.getActiveSession();
		if(_session == null){
			if(savedInstanceState != null){
				_session = Session.restoreSession(_activity, _tokenCachingStrategy, new Session.StatusCallback() {
					@Override
					public void call(Session session, SessionState state, Exception exception) {

					}
				}, savedInstanceState);
			}
			if(_session == null){
				Session.Builder sb = new Session.Builder(act);
				sb.setApplicationId(APP_ID);
				sb.setTokenCachingStrategy(_tokenCachingStrategy);
				_session = sb.build();
			}
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private Session.OpenRequest settingSessionRequest(){
		Session.OpenRequest so = new Session.OpenRequest(_activity);
		so.setPermissions(Arrays.asList(PERMISSIONS));
		so.setCallback(new Session.StatusCallback() {
			@Override
			public void call(Session session, SessionState state, Exception exception) {
			}
		});
		so.setIsLegacy(true);
		return so;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public boolean isLogin(){
		return _session.isOpened();
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void startLogin(){
		_session.openForRead(this.settingSessionRequest());
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void setLoginResult(int requestCode, int resultCode, Intent data){
		if(resultCode == Activity.RESULT_OK){
			_session.onActivityResult(_activity, requestCode, resultCode, data);
			if(_loginResultListener != null){
				if(_session.isOpened() && _session.getAccessToken() != null){
					_loginResultListener.success(_session.getAccessToken());
				}else{
					_loginResultListener.error();
				}
			}
		}else if(resultCode == Activity.RESULT_CANCELED){
			if(_loginResultListener != null){
				_loginResultListener.cancel();
			}
		}else{
			if(_loginResultListener != null){
				_loginResultListener.error();
			}
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void uploadMessage(String message) {
		if(_session.isOpened()){
			Request request = Request.newStatusUpdateRequest(_session, message, new Request.Callback() {
				@Override
				public void onCompleted(Response response) {
					if(_uploadListener != null){
						if(response.getError() == null){
							_uploadListener.success();
						}else{
							_uploadListener.error();
						}
					}
				}
			});
			RequestAsyncTask requestAsynk = new RequestAsyncTask(request);
			requestAsynk.execute();
		}else{
			if(_uploadListener != null){
				_uploadListener.error();
			}
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	private TokenCachingStrategy _tokenCachingStrategy = new TokenCachingStrategy() {

		@Override
		public void save(Bundle bundle) {
			Session.saveSession(_session, bundle);
		}

		@Override
		public Bundle load() {
			return null;
		}

		@Override
		public void clear() {
			_session.closeAndClearTokenInformation();
		}
	};

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * リスナーを追加する
	 */
	public void setOnLoginResultListener(LoginResultListener loginListener){
		_loginResultListener = loginListener;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * リスナーを削除する
	 */
	public void removeLoginResultListener(){
		_loginResultListener = null;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	//処理が終わったことを通知する独自のリスナーを作成
	public interface LoginResultListener extends EventListener {

		//Facebookの認証処理が完了し、AccessTokenが取得できる時に呼ばれる
		public void success(String accessToken);

		//Facebookの認証がキャンセルされた場合に呼ばれる
		public void cancel();

		//Facebookの認証時にエラーが発生した場合に呼ばれる
		public void error();

	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * リスナーを追加する
	 */
	public void setOnUploadListener(UploadListener uploadListener){
		_uploadListener = uploadListener;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * リスナーを削除する
	 */
	public void removeUploadListener(){
		_uploadListener = null;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------

	//処理が終わったことを通知する独自のリスナーを作成
	public interface UploadListener extends EventListener {

		//Facebookへの画像の投稿が完了した場合に呼ばれる
		public void success();

		//Facebookへの画像の投稿に失敗した場合に呼ばれる
		public void error();

	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------
}
